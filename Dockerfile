FROM peuybul/php-8.1.2-apache-sqlsrv:latest

RUN apt-get update
RUN apt-get install htop nano git -y

# Add File
COPY storage/config/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY storage/config/apache2.conf /etc/apache2/apache2.conf
COPY storage/config/php.ini /usr/local/etc/php/php.ini
COPY storage/www/ /var/www/html

WORKDIR /var/www/html

CMD ["apache2-foreground"]